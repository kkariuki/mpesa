package com.sycom.sypay.integration.mpesa.utils.feign;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.sycom.sypay.integration.mpesa.domains.dto.c2b.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Created by kevin kariuki on 7/21/17.
 */
@Service
public class MPESAC2BService {

    private static final Logger logger = LoggerFactory.getLogger(MPESAC2BService.class);

    private final MPESAC2BFeignClient mpesaFeignClient;

    public MPESAC2BService(MPESAC2BFeignClient mpesaFeignClient) {
        this.mpesaFeignClient = mpesaFeignClient;
    }

    @HystrixCommand(fallbackMethod = "registerURLsFallback")
    public Optional<URLRegistrationResponse> registerURLs(URLRegistrationRequest registrationRequest) {
        return Optional.of(mpesaFeignClient.registerConfirmationAndValidationURLs(registrationRequest));
    }

    public Optional<URLRegistrationResponse> registerURLsFallback(URLRegistrationRequest registrationRequest) {
        return Optional.empty();
    }

    @HystrixCommand(fallbackMethod = "simulateTransactionFallback")
    public Optional<TransactionResponse> simulateTransaction(TransactionRequest request) {
        return Optional.of(mpesaFeignClient.simulateTransaction(request));
    }

    public Optional<TransactionResponse> simulateTransactionFallback(TransactionRequest request) {
        return Optional.empty();
    }
}

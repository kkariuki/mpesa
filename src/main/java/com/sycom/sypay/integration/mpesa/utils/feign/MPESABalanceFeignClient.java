package com.sycom.sypay.integration.mpesa.utils.feign;

import com.sycom.sypay.integration.mpesa.configurations.MPESAFeignClientConfiguration;
import com.sycom.sypay.integration.mpesa.domains.dto.balance.CheckBalanceRequest;
import com.sycom.sypay.integration.mpesa.domains.dto.balance.CheckBalanceResponse;
import com.sycom.sypay.integration.mpesa.domains.dto.c2b.TransactionRequest;
import com.sycom.sypay.integration.mpesa.domains.dto.c2b.TransactionResponse;
import com.sycom.sypay.integration.mpesa.domains.dto.c2b.URLRegistrationRequest;
import com.sycom.sypay.integration.mpesa.domains.dto.c2b.URLRegistrationResponse;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.constraints.NotNull;

/**
 * Created by kevin kariuki on 7/21/17.
 */
@FeignClient(
        name = "mpesa",
        url = "${custom.integrations.safaricom-mpesa.urls.balance}",
        configuration = MPESAFeignClientConfiguration.class
)
public interface MPESABalanceFeignClient {

    @RequestMapping(method = RequestMethod.POST,
            value = "/query",
            consumes = "application/json",
            produces = "application/json")
    CheckBalanceResponse checkBalance(@NotNull CheckBalanceRequest request);
}

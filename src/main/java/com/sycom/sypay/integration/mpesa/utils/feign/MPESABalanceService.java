package com.sycom.sypay.integration.mpesa.utils.feign;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.sycom.sypay.integration.mpesa.domains.dto.balance.CheckBalanceRequest;
import com.sycom.sypay.integration.mpesa.domains.dto.balance.CheckBalanceResponse;
import com.sycom.sypay.integration.mpesa.domains.dto.c2b.TransactionRequest;
import com.sycom.sypay.integration.mpesa.domains.dto.c2b.TransactionResponse;
import com.sycom.sypay.integration.mpesa.domains.dto.c2b.URLRegistrationRequest;
import com.sycom.sypay.integration.mpesa.domains.dto.c2b.URLRegistrationResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Created by kevin kariuki on 7/21/17.
 */
@Service
public class MPESABalanceService {

    private static final Logger logger = LoggerFactory.getLogger(MPESABalanceService.class);

    private final MPESABalanceFeignClient mpesaBalanceFeignClient;

    public MPESABalanceService(MPESABalanceFeignClient mpesaBalanceFeignClient) {
        this.mpesaBalanceFeignClient = mpesaBalanceFeignClient;
    }

    @HystrixCommand(fallbackMethod = "checkBalanceFallback")
    public Optional<CheckBalanceResponse> checkBalance(CheckBalanceRequest request) {
        return Optional.of(mpesaBalanceFeignClient.checkBalance(request));
    }

    public Optional<CheckBalanceResponse> checkBalanceFallback(CheckBalanceRequest request) {
        return Optional.empty();
    }
}

package com.sycom.sypay.integration.mpesa.domains.dto.c2b;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * Created by kevin kariuki on 7/23/17.
 */
@Getter
@Setter
@ToString
public class ConfirmationResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("ResultCode")
    private String code;

    @JsonProperty("ResultDescription")
    private String description;

}
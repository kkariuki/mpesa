package com.sycom.sypay.integration.mpesa.utils.feign;

import com.sycom.sypay.integration.mpesa.configurations.MPESAFeignClientConfiguration;
import com.sycom.sypay.integration.mpesa.domains.dto.query.TransactionStatusRequest;
import com.sycom.sypay.integration.mpesa.domains.dto.reversal.TransactionReversalRequest;
import com.sycom.sypay.integration.mpesa.domains.dto.reversal.TransactionReversalResponse;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by kevin kariuki on 7/21/17.
 */
@FeignClient(
        name = "mpesa",
        url = "${custom.integrations.safaricom-mpesa.urls.transaction-status}",
        configuration = MPESAFeignClientConfiguration.class
)
public interface MPESACheckTransactionStatusFeignClient {

    @RequestMapping(method = RequestMethod.POST,
            value = "/query",
            consumes = "application/json",
            produces = "application/json")
    void queryTransactionStatus(TransactionStatusRequest request);
}

package com.sycom.sypay.integration.mpesa.configurations;

import feign.Feign;
import feign.Logger;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.client.OAuth2RestOperations;

/**
 * Feign Client that will always add OAuth2 access_token to Feign request header
 *
 * Created by kevin kariuki on 7/21/17.
 */
@Configuration
public class MPESAFeignClientConfiguration {

    @Qualifier("oauth2RestTemplate")
    private final OAuth2RestOperations restTemplate;

    public MPESAFeignClientConfiguration(OAuth2RestOperations restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Bean
    Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }

    @Bean
    public Feign.Builder feignBuilder(){
        return Feign.builder()
                .requestInterceptor(new OAuth2FeignRequestInterceptor(restTemplate.getAccessToken().toString()));
    }

    public static class OAuth2FeignRequestInterceptor implements RequestInterceptor {

        private static final String AUTHORIZATION_HEADER = "Authorization";

        private static final String BEARER_TOKEN_TYPE = "Bearer";

        private final String headerValue;

        private OAuth2FeignRequestInterceptor(String headerValue) {
            this.headerValue = headerValue;
        }

        @Override
        public void apply(RequestTemplate template) {
            template.header(AUTHORIZATION_HEADER,
                    String.format("%s %s", BEARER_TOKEN_TYPE, headerValue));
        }
    }

}

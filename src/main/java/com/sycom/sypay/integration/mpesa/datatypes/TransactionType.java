package com.sycom.sypay.integration.mpesa.datatypes;

/**
 * Created by kevin kariuki on 7/23/17.
 */
public enum TransactionType {
    CheckIdentity, CustomerPayBillOnline, CustomerBuyGoodsOnline
}

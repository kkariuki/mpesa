package com.sycom.sypay.integration.mpesa.utils.feign;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.sycom.sypay.integration.mpesa.domains.dto.query.TransactionStatusRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Created by kevin kariuki on 7/21/17.
 */
@Service
public class MPESACheckTransactionStatusService {

    private static final Logger logger = LoggerFactory.getLogger(MPESACheckTransactionStatusService.class);

    private final MPESACheckTransactionStatusFeignClient checkTransactionStatusFeignClient;

    public MPESACheckTransactionStatusService(MPESACheckTransactionStatusFeignClient checkTransactionStatusFeignClient) {
        this.checkTransactionStatusFeignClient = checkTransactionStatusFeignClient;
    }

    @HystrixCommand(fallbackMethod = "queryTransactionStatusFallback")
    public void queryTransactionStatus(TransactionStatusRequest request) {
        checkTransactionStatusFeignClient.queryTransactionStatus(request);
    }

    public void queryTransactionStatusFallback(TransactionStatusRequest request) {}
}

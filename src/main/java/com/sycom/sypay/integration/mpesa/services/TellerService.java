package com.sycom.sypay.integration.mpesa.services;

import com.sycom.sypay.integration.mpesa.domains.dto.c2b.TransactionRequest;
import com.sycom.sypay.integration.mpesa.domains.dto.c2b.URLRegistrationRequest;
import com.sycom.sypay.integration.mpesa.domains.dto.reversal.TransactionReversalRequest;
import com.sycom.sypay.integration.mpesa.domains.dto.stk.STKTransactionRequest;

/**
 * Created by kevin kariuki on 7/21/17.
 */
public interface TellerService {

    void registerCallbackURLs(URLRegistrationRequest request);

    void initiateC2BTransaction(TransactionRequest request);

    void initializeSTKTransaction(STKTransactionRequest request);

    void initializeTransactionReversal(TransactionReversalRequest request);

}

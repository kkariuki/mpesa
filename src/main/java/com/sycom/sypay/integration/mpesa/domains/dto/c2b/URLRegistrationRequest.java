package com.sycom.sypay.integration.mpesa.domains.dto.c2b;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sycom.sypay.integration.mpesa.datatypes.ResponseType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * This represents a confirmation and validation URL registration request body
 *
 * ValidationURL - Validation URL for the client
 * ConfirmationURL - Confirmation URL for the client
 * ResponseType - Default response type for timeout
 * ShortCode - The short code of the organization
 *
 *  {
 *      "ShortCode": " " ,
 *      "ResponseType": " ",
 *      "ConfirmationURL": " ",
 *      "ValidationURL": " "
 *  }
 *
 * Created by kevin kariuki on 7/21/17.
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
public class URLRegistrationRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("ShortCode")
    private String shortCode;

    @JsonProperty("ValidationURL")
    private String validationURL;

    @JsonProperty("ConfirmationURL")
    private String confirmationURL;

    @JsonProperty("ResponseType")
    private ResponseType responseType;

}

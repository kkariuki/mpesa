package com.sycom.sypay.integration.mpesa.domains.dto.c2b;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sycom.sypay.integration.mpesa.datatypes.CommandID;
import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * This represents a simulate request body
 *
 * CommandID - Unique command for each transaction type
 * Amount - The amount being transacted
 * Msisdn - Phone number (msisdn) initiating the transaction
 * BillRefNumber - Bill Reference Number (Optional)
 * ShortCode - Short Code receiving the amount being transacted
 *
 * {
 *    "ShortCode": "",
 *    "CommandID": "CustomerPayBillOnline",
 *    "Amount": "",
 *    "Msisdn": "",
 *    "BillRefNumber": ""
 *    }
 *
 *
 * Created by kevin kariuki on 7/21/17.
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
public class TransactionRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("ShortCode")
    private String shortCode;

    @JsonProperty("CommandID")
    private CommandID commandID;

    @JsonProperty("Amount")
    private BigDecimal transactionAmount;

    @JsonProperty("Msisdn")
    private String phoneNumber;

    @JsonProperty("BillRefNumber")
    private String paymentReference;

}

package com.sycom.sypay.integration.mpesa.domains.dto.balance;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sycom.sypay.integration.mpesa.datatypes.CommandID;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 *  Use this API to enquire the balance on an M-Pesa BuyGoods (Till Number).
 *
 *
 * Initiator - This is the credential/username used to authenticate the transaction request
 * SecurityCredential - Base256 encoded string of the M-Pesa short code and password, which is encrypted using M-Pesa public key and validates the transaction on M-Pesa Core system.
 * CommandID - A unique command passed to the M-Pesa system.
 * PartyB	The shortcode of the organisation receiving the transaction.
 * ReceiverIdentifierType	Type of the organisation receiving the transaction.
 * Remarks	Comments that are sent along with the transaction.
 * QueueTimeOutURL	The timeout end-point that receives a timeout message.
 * ResultURL	The end-point that receives a successful transaction.
 * AccountType	Organisation receiving the funds.
 *
 *  {
 *      "Initiator":"apiop5",
 *      "SecurityCredential":"security credential",
 *      "CommandID":"AccountBalance",
 *      "PartyA":"shortcode",
 *      "IdentifierType":"4",
 *      "Remarks":"Remarks",
 *      "QueueTimeOutURL":"https://ip_address:port/timeout_url",
 *      "ResultURL":"https://ip_address:port/result_url"
 *  }
 *
 * Created by kevin kariuki on 7/23/17.
 */
@Getter
@Setter
@ToString
public class CheckBalanceRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("Initiator")
    private String initiator;

    @JsonProperty("SecurityCredential")
    private String securityCredential;

    @JsonProperty("CommandID")
    private CommandID commandID;

    @JsonProperty("PartyA")
    private String partyA;

    @JsonProperty("IdentifierType")
    private int identifierType;

    @JsonProperty("Remarks")
    private String remarks;

    @JsonProperty("QueueTimeOutURL")
    private String queueTimeoutURL;

    @JsonProperty("ResultURL")
    private String resultURL;

    // ???
    /*@JsonProperty("accountType")
    private String accountType;*/

    public CheckBalanceRequest(CommandID commandID, String partyA, int identifierType, String remarks, String initiator,
                               String securityCredential, String queueTimeoutURL, String resultURL) {
        this.commandID = commandID;
        this.partyA = partyA;
        this.identifierType = identifierType;
        this.remarks = remarks;
        this.initiator = initiator;
        this.securityCredential = securityCredential;
        this.queueTimeoutURL = queueTimeoutURL;
        this.resultURL = resultURL;
    }

}

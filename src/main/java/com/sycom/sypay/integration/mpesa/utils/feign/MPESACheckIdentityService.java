package com.sycom.sypay.integration.mpesa.utils.feign;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.sycom.sypay.integration.mpesa.domains.dto.query.CheckIdentityRequest;
import com.sycom.sypay.integration.mpesa.domains.dto.query.CheckIdentityResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Created by kevin kariuki on 7/21/17.
 */
@Service
public class MPESACheckIdentityService {

    private static final Logger logger = LoggerFactory.getLogger(MPESACheckIdentityService.class);

    private final MPESACheckIdentityFeignClient mpesaCheckIdentityFeignClient;

    public MPESACheckIdentityService(MPESACheckIdentityFeignClient mpesaCheckIdentityFeignClient) {
        this.mpesaCheckIdentityFeignClient = mpesaCheckIdentityFeignClient;
    }

    @HystrixCommand(fallbackMethod = "queryIdentityFallback")
    public Optional<CheckIdentityResponse> queryIdentity(CheckIdentityRequest request) {
        return Optional.of(mpesaCheckIdentityFeignClient.queryIdentity(request));
    }

    public Optional<CheckIdentityResponse> queryIdentityFallback(CheckIdentityRequest request) {
        return Optional.empty();
    }
}

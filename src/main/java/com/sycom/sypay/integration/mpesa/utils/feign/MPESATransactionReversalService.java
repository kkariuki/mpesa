package com.sycom.sypay.integration.mpesa.utils.feign;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.sycom.sypay.integration.mpesa.domains.dto.balance.CheckBalanceRequest;
import com.sycom.sypay.integration.mpesa.domains.dto.reversal.TransactionReversalRequest;
import com.sycom.sypay.integration.mpesa.domains.dto.reversal.TransactionReversalResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Created by kevin kariuki on 7/21/17.
 */
@Service
public class MPESATransactionReversalService {

    private static final Logger logger = LoggerFactory.getLogger(MPESATransactionReversalService.class);

    private final MPESATransactionReversalFeignClient mpesaTransactionReversalFeignClient;

    public MPESATransactionReversalService(MPESATransactionReversalFeignClient mpesaTransactionReversalFeignClient) {
        this.mpesaTransactionReversalFeignClient = mpesaTransactionReversalFeignClient;
    }

    @HystrixCommand(fallbackMethod = "reverseTransactionFallback")
    public Optional<TransactionReversalResponse> reverseTransaction(TransactionReversalRequest request) {
        return Optional.of(mpesaTransactionReversalFeignClient.reverseTransaction(request));
    }

    public Optional<TransactionReversalResponse> reverseTransactionFallback(TransactionReversalRequest request) {
        return Optional.empty();
    }
}

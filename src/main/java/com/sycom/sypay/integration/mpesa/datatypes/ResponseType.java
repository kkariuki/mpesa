package com.sycom.sypay.integration.mpesa.datatypes;

public enum ResponseType {
    Completed, Cancelled
}

package com.sycom.sypay.integration.mpesa.datatypes;

/**
 * Identifier types - both sender and receiver - identify an M-Pesa transaction’s sending and receiving party as either
 * a shortcode, a till number or a MSISDN (phone number). There are three identifier types that can be used with
 * M-Pesa APIs.
 *
 * 1 - MSISDN
 * 2 - Till Number
 * 4 - Shortcode
 *
 * Created by kevin kariuki
 */
public enum IdentifierType {
    MSISDN(1),
    TILL_NUMBER(2),
    SHORT_CODE(4);

    private final int value;

    IdentifierType(final int newValue) {
        value = newValue;
    }

    public int getValue() { return value; }
}

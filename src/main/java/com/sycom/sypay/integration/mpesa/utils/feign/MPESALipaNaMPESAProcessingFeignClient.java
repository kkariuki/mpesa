package com.sycom.sypay.integration.mpesa.utils.feign;

import com.sycom.sypay.integration.mpesa.configurations.MPESAFeignClientConfiguration;
import com.sycom.sypay.integration.mpesa.domains.dto.stk.STKTransactionRequest;
import com.sycom.sypay.integration.mpesa.domains.dto.stk.STKTransactionResponse;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.constraints.NotNull;

/**
 * Created by kevin kariuki on 7/21/17.
 */
@FeignClient(
        name = "mpesa",
        url = "${custom.integrations.safaricom-mpesa.urls.lipa-na-mpesa-payment}",
        configuration = MPESAFeignClientConfiguration.class
)
public interface MPESALipaNaMPESAProcessingFeignClient {

    @RequestMapping(method = RequestMethod.POST,
            value = "/processrequest",
            consumes = "application/json",
            produces = "application/json")
    STKTransactionResponse simulateSTKTransaction(@NotNull STKTransactionRequest request);
}

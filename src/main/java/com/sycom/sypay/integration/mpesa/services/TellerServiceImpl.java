package com.sycom.sypay.integration.mpesa.services;

import com.sycom.sypay.integration.mpesa.domains.dto.c2b.TransactionRequest;
import com.sycom.sypay.integration.mpesa.domains.dto.c2b.TransactionResponse;
import com.sycom.sypay.integration.mpesa.domains.dto.c2b.URLRegistrationRequest;
import com.sycom.sypay.integration.mpesa.domains.dto.c2b.URLRegistrationResponse;
import com.sycom.sypay.integration.mpesa.domains.dto.reversal.TransactionReversalRequest;
import com.sycom.sypay.integration.mpesa.domains.dto.reversal.TransactionReversalResponse;
import com.sycom.sypay.integration.mpesa.domains.dto.stk.STKTransactionRequest;
import com.sycom.sypay.integration.mpesa.domains.dto.stk.STKTransactionResponse;
import com.sycom.sypay.integration.mpesa.utils.feign.MPESAC2BService;
import com.sycom.sypay.integration.mpesa.utils.feign.MPESALipaNaMPESAProcessingService;
import com.sycom.sypay.integration.mpesa.utils.feign.MPESATransactionReversalService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Base64;
import java.util.Optional;

/**
 * Created by kevin kariuki on 7/21/17.
 */
@Service
public class TellerServiceImpl implements TellerService {

    private static final Logger logger = LoggerFactory.getLogger(TellerServiceImpl.class);

    @Value("${custom.integrations.safaricom-mpesa.credentials.lipa-na-mpesa-online.shortcode}")
    private String stkInitiatorShortCode;

    @Value("${custom.integrations.safaricom-mpesa.credentials.lipa-na-mpesa-online.passkey}")
    private String passkey;

    private final MPESAC2BService mpesac2BService;
    private final MPESALipaNaMPESAProcessingService mpesaLipaNaMPESAProcessingService;
    private final MPESATransactionReversalService mpesaTransactionReversalService;

    public TellerServiceImpl(MPESAC2BService mpesac2BService, MPESALipaNaMPESAProcessingService mpesaLipaNaMPESAProcessingService,
                             MPESATransactionReversalService mpesaTransactionReversalService) {
        this.mpesac2BService = mpesac2BService;
        this.mpesaLipaNaMPESAProcessingService = mpesaLipaNaMPESAProcessingService;
        this.mpesaTransactionReversalService = mpesaTransactionReversalService;
    }

    @Override
    public void registerCallbackURLs(URLRegistrationRequest request) {
        final Optional<URLRegistrationResponse> registrationResponse = mpesac2BService.registerURLs(request);

        registrationResponse.ifPresent(response -> {
            logger.info("********************************************************");
            logger.info("Confirmation and Validation URL Registration");
            logger.info(response.getConversationID());
            logger.info(response.getOriginatorConversationID());
            logger.info(response.getResponseDescription());
            logger.info("********************************************************");
        });
    }

    @Override
    public void initiateC2BTransaction(TransactionRequest request) {
        final Optional<TransactionResponse> response = mpesac2BService.simulateTransaction(request);

        //TODO set the variable transactionID
        response.ifPresent(simulationResponse -> {
            logger.info("********************************************************");
            logger.info("Simulate Transaction");
            logger.info(simulationResponse.getConversationID());
            logger.info(simulationResponse.getOriginatorConversationID());
            logger.info(simulationResponse.getResponseDescription());
            logger.info("********************************************************");
        });
    }

    @Override
    public void initializeSTKTransaction(STKTransactionRequest request) {
        final Optional<STKTransactionResponse> response = mpesaLipaNaMPESAProcessingService.processPayment(this.secureSTKTransactionRequest(request));

        response.ifPresent(stkTransactionResponse -> {
            logger.info("********************************************************");
            logger.info("Simulate STK Transaction");
            logger.info(stkTransactionResponse.getMerchantRequestId());
            logger.info(stkTransactionResponse.getCheckoutRequestId());
            logger.info(stkTransactionResponse.getResponseDescription());
            logger.info(stkTransactionResponse.getResponseCode());
            logger.info(stkTransactionResponse.getCustomerMessage());
            logger.info("********************************************************");
        });
    }

    @Override
    public void initializeTransactionReversal(TransactionReversalRequest request) {
        final Optional<TransactionReversalResponse> response = mpesaTransactionReversalService.reverseTransaction(request);

        response.ifPresent(transactionReversalResponse -> {
            logger.info("*******************************************************");
            logger.info("Transaction Reversal");
            logger.info(transactionReversalResponse.getConversationID());
            logger.info(transactionReversalResponse.getOriginatorConversationID());
            logger.info(transactionReversalResponse.getResponseDescription());
            logger.info("*******************************************************");
        });
    }

    private STKTransactionRequest secureSTKTransactionRequest(STKTransactionRequest request) {
        System.out.println("this.stkInitiatorShortCode ");
        System.out.println(this.stkInitiatorShortCode );

        System.out.println("this.passkey ");
        System.out.println(this.passkey );


        System.out.println("this.getTimestamp ");
        System.out.println(request.getTimeStamp() );


        request.setPassword(Base64.getEncoder().encodeToString((this.stkInitiatorShortCode + this.passkey + request.getTimeStamp()).getBytes()));
        logger.info("*******************************************************");
        logger.info("*****************Password***********************");
        logger.info(request.getPassword());
        logger.info("*******************************************************");
        return request;
    }
}

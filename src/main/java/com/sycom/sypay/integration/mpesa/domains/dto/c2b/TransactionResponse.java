package com.sycom.sypay.integration.mpesa.domains.dto.c2b;

import com.sycom.sypay.integration.mpesa.domains.dto.Response;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by kevin kariuki on 7/23/17.
 */
@Getter
@Setter
@ToString
public class TransactionResponse extends Response {
}

package com.sycom.sypay.integration.mpesa.domains.dto.balance;

import com.sycom.sypay.integration.mpesa.domains.dto.Response;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * OriginatorConverstionID - The unique request ID for tracking a transaction	Alpha-Numeric	alpha-numeric string of less then 20 characters
 * ConversationID - The unique request ID returned by mpesa for each request made	Alpha-Numeric - Error codes - 500 OK
 * ResponseDescription - Response Description message	String - The service request has failed - The service request has been accepted successfully
 *
 * Created by kevin kariuki on 7/23/17.
 */
@Getter
@Setter
@ToString
public class CheckBalanceResponse extends Response {
}
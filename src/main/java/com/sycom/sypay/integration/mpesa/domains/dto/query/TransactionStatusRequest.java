package com.sycom.sypay.integration.mpesa.domains.dto.query;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sycom.sypay.integration.mpesa.datatypes.CommandID;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 *  Transaction Status API checks the status of a B2B, B2C and C2B APIs transactions.
 *
 * CommandID - Unique command for each transaction type, possible values are: TransactionStatusQuery.
 * ShortCode - Organization /MSISDN sending the transaction.
 * IdentifierType - Type of organization receiving the transaction
 * Remarks - Comments that are sent along with the transaction.
 * Initiator - The name of Initiator to initiating the request.
 * SecurityCredential - Base256 encoded string of the M-Pesa short code and password, which is encrypted using M-Pesa public key and validates the transaction on M-Pesa Core system.
 * QueueTimeOutURL - The path that stores information of time out transaction.
 * ResultURL - The path that stores information of transaction.
 * TransactionID - Organization Receiving the funds.
 * Occasion	- Optional.
 *
 *
 *  {
 *      "Initiator":" ",
 *      "SecurityCredential":" ",
 *      "CommandID":"TransactionStatusQuery",
 *      "TransactionID":" ",
 *      "PartyA":" ",
 *      "IdentifierType":"1",
 *      "ResultURL":"https://ip_address:port/result_url",
 *      "QueueTimeOutURL":"https://ip_address:port/timeout_url",
 *      "Remarks":" ",
 *      "Occasion":" "
 *  }
 *
 * Created by kevin kariuki on 7/23/17.
 */
@Getter
@Setter
@ToString
public class TransactionStatusRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("Initiator")
    private String initiator;

    @JsonProperty("SecurityCredential")
    private String securityCredential;

    @JsonProperty("CommandID")
    private CommandID command;

    @JsonProperty("TransactionID")
    private String transactionId;

    @JsonProperty("PartyA")
    private String partyA;

    @JsonProperty("IdentifierType")
    private int identifierType;

    @JsonProperty("Remarks")
    private String remarks;

    @JsonProperty("QueueTimeOutURL")
    private String queueTimeoutURL;

    @JsonProperty("ResultURL")
    private String resultURL;

    @JsonProperty("Occasion")
    private String occasion;

    public TransactionStatusRequest(CommandID commandID, String initiator, String securityCredential, String partyA,
                                    int identifierType, String remarks, String transactionId, String queueTimeoutURL,
                                    String resultURL) {
        this.command = commandID;
        this.initiator = initiator;
        this.securityCredential = securityCredential;
        this.partyA = partyA;
        this.identifierType = identifierType;
        this.remarks = remarks;
        this.transactionId = transactionId;
        this.queueTimeoutURL = queueTimeoutURL;
        this.resultURL = resultURL;
    }
}

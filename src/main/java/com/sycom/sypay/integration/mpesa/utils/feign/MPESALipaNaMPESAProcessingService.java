package com.sycom.sypay.integration.mpesa.utils.feign;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.sycom.sypay.integration.mpesa.domains.dto.stk.STKTransactionRequest;
import com.sycom.sypay.integration.mpesa.domains.dto.stk.STKTransactionResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Created by kevin kariuki on 7/21/17.
 */
@Service
public class MPESALipaNaMPESAProcessingService {

    private static final Logger logger = LoggerFactory.getLogger(MPESALipaNaMPESAProcessingService.class);

    private final MPESALipaNaMPESAProcessingFeignClient mpesaFeignClient;

    public MPESALipaNaMPESAProcessingService(MPESALipaNaMPESAProcessingFeignClient mpesaFeignClient) {
        this.mpesaFeignClient = mpesaFeignClient;
    }

    @HystrixCommand(fallbackMethod = "processPaymentFallback")
    public Optional<STKTransactionResponse> processPayment(STKTransactionRequest transactionRequest) {
        return Optional.of(mpesaFeignClient.simulateSTKTransaction(transactionRequest));
    }

    public Optional<STKTransactionResponse> processPaymentFallback(STKTransactionRequest transactionRequest) {
        return Optional.empty();
    }

}

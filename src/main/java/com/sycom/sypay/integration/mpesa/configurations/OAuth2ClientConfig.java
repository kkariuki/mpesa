package com.sycom.sypay.integration.mpesa.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpMethod;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsAccessTokenProvider;

import java.io.IOException;

/**
 * Created by kevin kariuki on 7/21/17.
 */
@Configuration
public class OAuth2ClientConfig {

    @Bean
    @Primary
    public OAuth2RestTemplate oauth2RestTemplate(OAuth2ClientContext oauth2ClientContext,
                                                 OAuth2ProtectedResourceDetails details) throws IOException {

        OAuth2RestTemplate oAuth2RestTemplate = new OAuth2RestTemplate(details, oauth2ClientContext);
        CustomClientCredentialsAccessTokenProvider tokenProvider = new CustomClientCredentialsAccessTokenProvider();
        oAuth2RestTemplate.setAccessTokenProvider(tokenProvider);
        return oAuth2RestTemplate;
    }

}

class CustomClientCredentialsAccessTokenProvider extends ClientCredentialsAccessTokenProvider {
    @Override
    protected HttpMethod getHttpMethod() {
        return HttpMethod.GET;
    }
}

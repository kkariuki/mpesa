package com.sycom.sypay.integration.mpesa.utils.feign;

import com.sycom.sypay.integration.mpesa.configurations.MPESAFeignClientConfiguration;
import com.sycom.sypay.integration.mpesa.domains.dto.c2b.*;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.constraints.NotNull;

/**
 * Created by kevin kariuki on 7/21/17.
 */
@FeignClient(
        name = "mpesa",
        url = "${custom.integrations.safaricom-mpesa.urls.c2b}",
        configuration = MPESAFeignClientConfiguration.class
)
public interface MPESAC2BFeignClient {

    @RequestMapping(method = RequestMethod.POST,
            value = "/simulate",
            consumes = "application/json",
            produces = "application/json")
    TransactionResponse simulateTransaction(@NotNull TransactionRequest request);

    @RequestMapping(method = RequestMethod.POST,
            value = "/registerurl",
            consumes = "application/json",
            produces = "application/json")
    URLRegistrationResponse registerConfirmationAndValidationURLs(@NotNull URLRegistrationRequest registrationRequest);
}

package com.sycom.sypay.integration.mpesa.domains.dto.stk;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sycom.sypay.integration.mpesa.datatypes.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * BusinessShortCode -  The organization shortcode used to receive the transaction  Numeric	- Shortcode (6 digits)
 * Password - The password for encrypting the request	String	base64.encode(Shortcode:Passkey:Timestamp)
 * Timestamp - The timestamp of the transaction	Timestamp	yyyymmddhhiiss
 * TransactionType - The transaction type to be used for the request 'CustomerPayBillOnline'	String	CustomerPayBillOnline
 * Amount - The amount to be transacted	Numeric	1
 * PartyA - The entity sending the funds	Numeric	MSISDN (12 digits)
 * PartyB - The organization receiving the funds Numeric	- Shortcode (6 digits)
 * PhoneNumber - The MSISDN sending the funds	Numeric	MSISDN (12 digits)
 * CallBackURL - Call Back URL	URL	https://ip or domain:port/path
 * AccountReference - Account Reference	Alpha-Numeric	Any combinations of letters and numbers
 * TransactionDesc - Description of the transaction	String	any string of less then 20 characters
 *
 * Created by kevin kariuki on 8/01/17.
 */
@Getter
@Setter
@AllArgsConstructor
public class STKTransactionRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("BusinessShortCode")
    private String businessShortCode;

    @JsonProperty("Password")
    private String password;

    @JsonProperty("Timestamp")
    private Date timeStamp = new Date();

    @JsonProperty("TransactionType")
    private TransactionType transactionType;

    @JsonProperty("Amount")
    private BigDecimal amount;

    @JsonProperty("PartyA")
    private String sender;

    @JsonProperty("PartyB")
    private String recipient;

    @JsonProperty("PhoneNumber")
    private String phoneNumber;

    @JsonProperty("CallBackURL")
    private String callBackURL;

    @JsonProperty("AccountReference")
    private String accountReference;

    @JsonProperty("TransactionDesc")
    private String transactionDescription;

    public STKTransactionRequest(String businessShortCode, TransactionType transactionType, BigDecimal amount,
                                 String sender, String recipient, String phoneNumber, String callBackURL, String accountReference,
                                 String transactionDescription) {
        this.businessShortCode = businessShortCode;
        this.transactionType = transactionType;
        this.amount = amount;
        this.sender = sender;
        this.recipient = recipient;
        this.phoneNumber = phoneNumber;
        this.callBackURL = callBackURL;
        this.accountReference = accountReference;
        this.transactionDescription = transactionDescription;
    }

    public String getTimeStamp() {
        final SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss");
        return DATE_TIME_FORMAT.format(this.timeStamp);
    }

}

package com.sycom.sypay.integration.mpesa.domains.dto.stk;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * MerchantRequestID - Merchant Request ID	Numeric	1234-1234-1
 * CheckoutRequestID - Checkout Request ID	String	ws_co_123456789
 * ResponseDescription - Response Description message	String  -The service request has failed  -The service request has been accepted successfully
 * ResponseCode - Response Code	Numeric - 0  Error codes
 * CustomerMessage - Customer Message	String	A sequence of less then 20 characters
 *
 *
 * Created by kevin kariuki 8/1/2017
 */

@Getter
@Setter
@ToString
public class STKTransactionResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("MerchantRequestID")
    private String merchantRequestId;

    @JsonProperty("CheckoutRequestID")
    private String checkoutRequestId;

    @JsonProperty("ResponseDescription")
    private String responseDescription;

    @JsonProperty("ResponseCode")
    private String responseCode;

    @JsonProperty("CustomerMessage")
    private String customerMessage;
}

package com.sycom.sypay.integration.mpesa.domains.dto.transactionstatus;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * Transaction Status API checks the status of a B2B, B2C and C2B APIs transactions.
 *
 * CommandID	Unique command for each transaction type, possible values are: TransactionStatusQuery.
 * ShortCode	Organization /MSISDN sending the transaction.
 * IdentifierType	Type of organization receiving the transaction
 * Remarks	Comments that are sent along with the transaction.
 * Initiator	The name of Initiator to initiating the request.
 * SecurityCredential	Base64 encoded string of the M-Pesa short code and password, which is encrypted using M-Pesa public key and validates the transaction on M-Pesa Core system.
 * QueueTimeOutURL	The path that stores information of time out transaction.
 * ResultURL	The path that stores information of transaction.
 * TransactionID	Organization Receiving the funds.
 * Occasion	Optional.
 *
 *
 */
@Getter
@Setter
@ToString
public class TransactionStatusRequest {


}

package com.sycom.sypay.integration.mpesa.domains.dto.stk;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * // A cancelled request
 * {
 *   "Body":{
 *        "stkCallback":{
 *           "MerchantRequestID":"8555-67195-1",
 *           "CheckoutRequestID":"ws_CO_27072017151044001",
 *           "ResultCode":1032,
 *           "ResultDesc":"[STK_CB - ]Request cancelled by user"
 *        }
 *   }
 * }
 *
 * // An accepted request
 * {
 *      "Body":{
 *          "stkCallback":{
 *              "MerchantRequestID":"19465-780693-1",
 *              "CheckoutRequestID":"ws_CO_27072017154747416",
 *              "ResultCode":0,
 *              "ResultDesc":"The service request is processed successfully.",
 *              "CallbackMetadata":{
 *                  "Item":[
 *                      {
 *                          "Name":"Amount",
 *                          "Value":1
 *                      },
 *                      {
 *                          "Name":"MpesaReceiptNumber",
 *                          "Value":"LGR7OWQX0R"
 *                      },
 *                      {
 *                          "Name":"Balance"
 *                      },
 *                      {
 *                          "Name":"TransactionDate",
 *                          "Value":20170727154800
 *                      },
 *                      {
 *                          "Name":"PhoneNumber",
 *                          "Value":254721566839
 *                      }
 *                  ]
 *              }
 *          }
 *      }
 *  }
 */

@Getter
@Setter
@ToString
public class STKTransactionCallbackRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("Body")
    private STKTransactionCallbackRequestBody body;

    @Getter
    @Setter
    @ToString
    public static class STKTransactionCallbackRequestBody  implements Serializable {

        private static final long serialVersionUID = 1L;

        @JsonProperty("stkCallback")
        private STKTransactionCallback callback;

        @Getter
        @Setter
        @ToString
        public static class STKTransactionCallback implements Serializable {

            private static final long serialVersionUID = 1L;

            @JsonProperty("MerchantRequestID")
            private String merchantRequestId;

            @JsonProperty("CheckoutRequestID")
            private String checkOutRequestId;

            @JsonProperty("ResultCode")
            private String resultCode;

            @JsonProperty("ResultDesc")
            private String resultDescription;

            @JsonProperty("CallbackMetadata")
            private CallbackMetadata metadata;

            @Getter
            @Setter
            @ToString
            public static class CallbackMetadata implements Serializable {

                private static final long serialVersionUID = 1L;

                @JsonProperty("Item")
                private List<CallbackMetadataItem> items = new ArrayList<>();

                @Getter
                @Setter
                @ToString
                public static class CallbackMetadataItem implements Serializable {

                    private static final long serialVersionUID = 1L;

                    @JsonProperty("Name")
                    private String name;

                    @JsonProperty("Value")
                    private String value;

                }
            }
        }
    }
}
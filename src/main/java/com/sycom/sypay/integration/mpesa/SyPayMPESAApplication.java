package com.sycom.sypay.integration.mpesa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;

@EnableCircuitBreaker
@EnableConfigurationProperties
//@EnableEurekaClient
@EnableFeignClients
@EnableHystrix
@EnableOAuth2Client
@SpringBootApplication
public class SyPayMPESAApplication {

	public static void main(String[] args) {
		SpringApplication.run(SyPayMPESAApplication.class, args);
	}

}
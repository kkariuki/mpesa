package com.sycom.sypay.integration.mpesa.domains.dto.reversal;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sycom.sypay.integration.mpesa.datatypes.CommandID;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Reverses a B2B, B2C or C2B M-Pesa transaction.
 *
 * Initiator - This is the credential/username used to authenticate the transaction request.
 * SecurityCredential - Base256 encoded string of the M-Pesa short code and password, which is encrypted using M-Pesa public key and validates the transaction on M-Pesa Core system.
 * CommandID - Unique command for each transaction type, possible values are: TransactionReversal.
 * PartyA - Organization/MSISDN sending the transaction.
 * RecieverIdentifierType - Type of organization receiving the transaction.
 * Remarks - Comments that are sent along with the transaction.
 * QueueTimeOutURL - The path that stores information of time out transaction.
 * ResultURL - The path that stores information of transaction.
 * TransactionID - Organization Receiving the funds.
 * Occasion - Optional.
 *
 *  {
 *      "Initiator":"apiop5",
 *      "SecurityCredential":" ",
 *      "CommandID":"TransactionReversal",
 *      "TransactionID":" ",
 *      "Amount":" ",
 *      "ReceiverParty":" ",
 *      "RecieverIdentifierType":"4",
 *      "ResultURL":"https://ip_address:port/result_url",
 *      "QueueTimeOutURL":"https://ip_address:port/timeout_url",
 *      "Remarks":" ",
 *      "Occasion":" "
 *  }
 *
 * Created by kevin kariuki on 7/23/17.
 */
@Getter
@Setter
@ToString
public class TransactionReversalRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("Initiator")
    private String initiator;

    @JsonProperty("SecurityCredential")
    private String securityCredential;

    @JsonProperty("CommandID")
    private CommandID command;

    @JsonProperty("TransactionID")
    private String transactionId;

    @JsonProperty("Amount")
    private BigDecimal amount;

    @JsonProperty("ReceiverParty")
    private String receiverParty;

    @JsonProperty("RecieverIdentifierType")
    private String receiverIdentifierParty;

    @JsonProperty("ResultURL")
    private String resultURL;

    @JsonProperty("QueueTimeOutURL")
    private String queueTimeOutURL;

    @JsonProperty("Remarks")
    private String remarks;

    @JsonProperty("Occasion")
    private String occassion;

}

package com.sycom.sypay.integration.mpesa.utils.feign;

import com.sycom.sypay.integration.mpesa.configurations.MPESAFeignClientConfiguration;
import com.sycom.sypay.integration.mpesa.domains.dto.balance.CheckBalanceRequest;
import com.sycom.sypay.integration.mpesa.domains.dto.reversal.TransactionReversalRequest;
import com.sycom.sypay.integration.mpesa.domains.dto.reversal.TransactionReversalResponse;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.constraints.NotNull;

/**
 * Created by kevin kariuki on 7/21/17.
 */
@FeignClient(
        name = "mpesa",
        url = "${custom.integrations.safaricom-mpesa.urls.reversal}",
        configuration = MPESAFeignClientConfiguration.class
)
public interface MPESATransactionReversalFeignClient {

    @RequestMapping(method = RequestMethod.POST,
            value = "/request",
            consumes = "application/json",
            produces = "application/json")
    TransactionReversalResponse reverseTransaction(TransactionReversalRequest request);
}

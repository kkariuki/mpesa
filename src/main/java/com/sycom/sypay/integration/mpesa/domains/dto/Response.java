package com.sycom.sypay.integration.mpesa.domains.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * This represents a request response
 *
 * OriginatorConverstionID - The unique request ID for tracking a transaction
 * ConversationID - The unique request ID returned by mpesa for each request made
 * ResponseDescription - Response Description message
 *
 * Created by kevin kariuki on 7/21/17.
 */
@Getter
@Setter
@ToString
public abstract class Response implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("OriginatorCoversationID")
    private String originatorConversationID;

    @JsonProperty("ConversationID")
    private String conversationID;

    @JsonProperty("ResponseDescription")
    private String responseDescription;

}

package com.sycom.sypay.integration.mpesa.domains.dto.query;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sycom.sypay.integration.mpesa.datatypes.TransactionType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * BusinessShortCode - Business Short Code
 * Password	- Password
 * Timestamp - Timestamp
 * TransactionType - Transaction Type
 * PhoneNumber - Safaricom phone number of the subscriber whose identity is being checked.
 * CallBackURL - Call Back URL
 * TransactionDesc - Transaction description
 *
 *  {
 *      "BusinessShortCode":" ",
 *      "Password":" ",
 *      "Timestamp":" ",
 *      "TransactionType":"CheckIdentity",
 *      "PhoneNumber":" ",
 *      "CallBackURL":"https://ip_address:port/result_url",
 *      "TransactionDesc":" "
 *  }
 *
 * Created by kevin kariuki on 7/23/17.
 */
@Getter
@Setter
@ToString
public class CheckIdentityRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("BusinessShortCode")
    private String shortCode;

    @JsonProperty("Password")
    private String password;

    @JsonProperty("Timestamp")
    private Date timeStamp = new Date();

    @JsonProperty("TransactionType")
    private TransactionType transactionType;

    @JsonProperty("PhoneNumber")
    private String phoneNumber;

    @JsonProperty("CallBackURL")
    private String callBackURL;

    @JsonProperty("TransactionDesc")
    private String transactionDescription;

    public CheckIdentityRequest(String shortCode, String password, TransactionType transactionType, String phoneNumber,
                                String callBackURL, String transactionDescription) {
        this.shortCode = shortCode;
        this.password = password;
        this.transactionType = transactionType;
        this.phoneNumber = phoneNumber;
        this.callBackURL = callBackURL;
        this.transactionDescription = transactionDescription;
    }

    public String getTimeStamp() {
        final SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss");
        return DATE_TIME_FORMAT.format(this.timeStamp);
    }

}

package com.sycom.sypay.integration.mpesa.domains.dto.reversal;

import com.sycom.sypay.integration.mpesa.domains.dto.Response;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by kevin kariuki on 7/23/17.
 */
@Getter
@Setter
@ToString
public class TransactionReversalResponse extends Response {
}

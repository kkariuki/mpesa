package com.sycom.sypay.integration.mpesa.web;

import com.sycom.sypay.integration.mpesa.domains.dto.stk.STKTransactionCallbackRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Created by kevin kariuki on 7/23/17.
 */
@RestController
public class MPESACallBackRestController {

    private static final Logger logger = LoggerFactory.getLogger(MPESACallBackRestController.class);

    @GetMapping
    public void helloWorld() {
        logger.info("Helllllooooo");
    }

    @PostMapping("/query/balance/result")
    public void accountBalanceResultCallbackHandler() {
        logger.info("Hey! I have been hit bwana!!! Someone just checked till balance");
    }

    @PostMapping("/query/balance/timeout")
    public void accountBalanceTimeOutCallbackHandler() {
        logger.info("Hey! I have been hit bwana!!! Someone just checked till balance but an error occurred");
    }

    @PostMapping("/reversal/result")
    public void transactionReversalResultCallbackHandler() {
        logger.info("Hey! I have been hit bwana!!! Someone just reversed a transaction");
    }

    @PostMapping("/reversal/timeout")
    public void transactionReversalTimeOutCallbackHandler() {
        logger.info("Hey! I have been hit bwana!!! Someone just reversed a transaction but an error occurred");
    }

    @PostMapping("/c2b/transaction/confirmation")
    public void confirmationCallbackHandler() {
        logger.info("Hey! I have been hit bwana!!! Someone is sending a confirmation thingy");
    }

    @PostMapping("/c2b/transaction/validation")
    public void validationCallbackHandler() {
        logger.info("Hey! I have been hit bwana!!! Someone is sending a validation thingy");
    }

    @PostMapping("/query/identity/result")
    public void queryIdentityResultCallbackHandler() {
        logger.info("Hey! I have been hit bwana!!! Someone just queried for an identity");
    }

    @PostMapping("/query/identity/timeout")
    public void queryIdentityTimeOutCallbackHandler() {
        logger.info("Hey! I have been hit bwana!!! Someone just queried for an identity but an error occurred");
    }

    @PostMapping("/query/transaction/result")
    public void queryTransactionStatusResultCallbackHandler() {
        logger.info("Hey! I have been hit bwana!!! Someone just queried for a transaction status");
    }
//    2020 02 23 23 34 30

    @PostMapping("/query/transaction/timeout")
    public void queryTransactionStatusTimeOutCallbackHandler() {
        logger.info("Hey! I have been hit bwana!!! Someone just queried for a transaction status but an error occurred");
    }

    @PostMapping(value = "/stk/push")
    public void stkCallbackHandler(@RequestBody STKTransactionCallbackRequest request) {
        logger.info(request.toString());
        logger.info("Hey! I have been hit bwana!!! Someone just initiated an STK transaction");
    }

}
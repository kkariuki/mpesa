package com.sycom.sypay.integration.mpesa.utils.feign;

import com.sycom.sypay.integration.mpesa.configurations.MPESAFeignClientConfiguration;
import com.sycom.sypay.integration.mpesa.domains.dto.query.CheckIdentityRequest;
import com.sycom.sypay.integration.mpesa.domains.dto.query.CheckIdentityResponse;
import com.sycom.sypay.integration.mpesa.domains.dto.query.TransactionStatusRequest;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.constraints.NotNull;

/**
 * Created by kevin kariuki on 7/21/17.
 */
@FeignClient(
        name = "mpesa",
        url = "${custom.integrations.safaricom-mpesa.urls.check-identity}",
        configuration = MPESAFeignClientConfiguration.class
)
public interface MPESACheckIdentityFeignClient {

    @RequestMapping(method = RequestMethod.POST,
            value = "/processrequest",
            consumes = "application/json",
            produces = "application/json")
    CheckIdentityResponse queryIdentity(@NotNull CheckIdentityRequest request);
}

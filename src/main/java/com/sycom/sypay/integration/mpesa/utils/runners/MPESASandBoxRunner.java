package com.sycom.sypay.integration.mpesa.utils.runners;

import com.sycom.sypay.integration.mpesa.datatypes.CommandID;
import com.sycom.sypay.integration.mpesa.datatypes.IdentifierType;
import com.sycom.sypay.integration.mpesa.datatypes.ResponseType;
import com.sycom.sypay.integration.mpesa.datatypes.TransactionType;
import com.sycom.sypay.integration.mpesa.domains.dto.balance.CheckBalanceRequest;
import com.sycom.sypay.integration.mpesa.domains.dto.balance.CheckBalanceResponse;
import com.sycom.sypay.integration.mpesa.domains.dto.c2b.TransactionRequest;
import com.sycom.sypay.integration.mpesa.domains.dto.c2b.URLRegistrationRequest;
import com.sycom.sypay.integration.mpesa.domains.dto.query.CheckIdentityRequest;
import com.sycom.sypay.integration.mpesa.domains.dto.query.TransactionStatusRequest;
import com.sycom.sypay.integration.mpesa.domains.dto.reversal.TransactionReversalRequest;
import com.sycom.sypay.integration.mpesa.domains.dto.stk.STKTransactionRequest;
import com.sycom.sypay.integration.mpesa.services.TellerService;
import com.sycom.sypay.integration.mpesa.utils.feign.MPESABalanceService;
import com.sycom.sypay.integration.mpesa.utils.feign.MPESACheckIdentityService;
import com.sycom.sypay.integration.mpesa.utils.feign.MPESACheckTransactionStatusService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Optional;

/**
 * Created by kevin kariuki on 7/21/17.
 */
@Component
public class MPESASandBoxRunner implements CommandLineRunner {

    private static final Logger logger = LoggerFactory.getLogger(MPESASandBoxRunner.class);

    private String transactionID = "";

    private final MPESABalanceService mpesaBalanceService;
    private final MPESACheckIdentityService mpesaCheckIdentityService;
    private final MPESACheckTransactionStatusService mpesaCheckTransactionStatusService;
    private final TellerService tellerService;

    public MPESASandBoxRunner(MPESABalanceService mpesaBalanceService,
                              MPESACheckIdentityService mpesaCheckIdentityService, MPESACheckTransactionStatusService mpesaCheckTransactionStatusService,
                              TellerService tellerService) {
        this.mpesaBalanceService = mpesaBalanceService;
        this.mpesaCheckIdentityService = mpesaCheckIdentityService;
        this.mpesaCheckTransactionStatusService = mpesaCheckTransactionStatusService;
        this.tellerService = tellerService;
    }

    @Override
    public void run(String... args) throws Exception {
//        this.registerURLs();
//        this.checkIdentity();
        //this.checkBalance();
        //this.simulateC2BTransaction();
        //this.checkC2BTransactionStatus();
        //this.checkBalance();
        this.simulateSTKTransaction();
        //this.reverseTransaction();
        //this.checkTransactionStatus();
        //this.checkBalance();

    }

    private void checkBalance() {
        Optional<CheckBalanceResponse> checkBalanceResponse = mpesaBalanceService.checkBalance(
                new CheckBalanceRequest(CommandID.AccountBalance, "698489", IdentifierType.SHORT_CODE.getValue(),
                        "Balance enquiry", "apiop5", "ZNIdv0lQujk2Mvu4ALZILPdQQ4xkv58BRVI7pc2C+jey19BjoF86A0VeHNzqf1QfiMUboXVcjC4rWuqePRgXDlCAV993nDmJq1WnrFlnOcZO/IAnKKdm0KJhjq/a+P7cA4WqCZzjTpV8rxy2TKWNXYfbA/Ks8w2kupnCkQf6k6VOBLcIdzpyDHrrhBz4kilIf4D9x1/eoJXLjjQpwT1h99y6u+YgSTDYkFhdRGE7/PCQ2dxV0U0tYPahDhCoCAOcoiZ1bRD9WEOO0ED+FxlQ4819ijLicbDsj9spIz00+HLLwpW8/qq0f25XSA5feJn733ucPSXRfuHZ5RU7qV/63w==",
                        "http://41.212.59.124:8088/query/balance/timeout",
                        "http://41.212.59.124:8088/query/balance/result"));

        checkBalanceResponse.ifPresent(response -> {
            logger.info("********************************************************");
            logger.info("Check Balance Request");
            logger.info(response.getConversationID());
            logger.info(response.getOriginatorConversationID());
            logger.info(response.getResponseDescription());
            logger.info("********************************************************");
        });
    }

    private void checkIdentity() {
        mpesaCheckIdentityService.queryIdentity(new CheckIdentityRequest("698489", "",
                TransactionType.CheckIdentity, "254708374149", "http://41.212.59.124:8088/query/identity/result",
                "Identity check"));
    }

    private void checkC2BTransactionStatus() {
        mpesaCheckTransactionStatusService.queryTransactionStatus(
                new TransactionStatusRequest(CommandID.TransactionStatusQuery, "apiop5", "ZNIdv0lQujk2Mvu4ALZILPdQQ4xkv58BRVI7pc2C+jey19BjoF86A0VeHNzqf1QfiMUboXVcjC4rWuqePRgXDlCAV993nDmJq1WnrFlnOcZO/IAnKKdm0KJhjq/a+P7cA4WqCZzjTpV8rxy2TKWNXYfbA/Ks8w2kupnCkQf6k6VOBLcIdzpyDHrrhBz4kilIf4D9x1/eoJXLjjQpwT1h99y6u+YgSTDYkFhdRGE7/PCQ2dxV0U0tYPahDhCoCAOcoiZ1bRD9WEOO0ED+FxlQ4819ijLicbDsj9spIz00+HLLwpW8/qq0f25XSA5feJn733ucPSXRfuHZ5RU7qV/63w==",
                        "698489", IdentifierType.SHORT_CODE.getValue(), "Transaction status enquiry", transactionID,
                        "http://41.212.59.124:8088/query/transaction/timeout",
                        "http://41.212.59.124:8088/query/transaction/result"));
    }

    private void registerURLs() {
        tellerService.registerCallbackURLs(new URLRegistrationRequest("698489",
                "http://41.212.59.124:8088/c2b/transaction/validation",
                "http://41.212.59.124:8088/c2b/transaction/confirmation", ResponseType.Cancelled));
    }

    private void reverseTransaction() {
        tellerService.initializeTransactionReversal(new TransactionReversalRequest());
    }

    private void simulateC2BTransaction() {
        tellerService.initiateC2BTransaction(new TransactionRequest(
                "698489", CommandID.CustomerPayBillOnline, new BigDecimal(1000.00), "254708374149",
                "INV-001"));
    }

    private void simulateSTKTransaction() {
        tellerService.initializeSTKTransaction(new STKTransactionRequest(
                "764900", TransactionType.CustomerPayBillOnline, new BigDecimal(1.00),
                "254720730430","764900", "254720730430",
                "http://41.212.59.124:8088/stk",
                "INV-001", "Payment for INV-001"
        ));
    }

}

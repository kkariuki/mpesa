package com.sycom.sypay.integration.mpesa.domains.dto.query;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 *
 * ConverstionID - A unique alpha-numeric code generated by the API Gateway to track the API call.
 * CustomerMessage -Customer Message.
 * ResponseCode - A unique alpha-numeric code generated by the backend system responding to the API request.
 * MerchantRequestID - Merchant Request ID.
 * ResponseDescription - A descriptive message accompanying the response.
 * CheckoutRequestID - Checkout Request ID.
 *
 * Created by kevin kariuki on 7/23/17.
 */
@Getter
@Setter
@ToString
public class CheckIdentityResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("ConverstionID")
    private String conversationId;

    @JsonProperty("CustomerMessage")
    private String customerMessage;

    @JsonProperty("ResponseCode")
    private String responseCode;

    @JsonProperty("ResponseDescription")
    private String responseDescription;

    @JsonProperty("MerchantRequestID")
    private String merchantRequestId;

    @JsonProperty("CheckoutRequestID")
    private String checkOutRequestId;
}
